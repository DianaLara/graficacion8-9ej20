/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sierpinski;
import java.awt.*;
import java.applet.*;


/**
 *
 * @author Diana
 */
public class Sierpinski extends Applet 
{   
    
    public void Triangulo(Graphics g, int x1, int y1, int x2, int y2, int x3, int y3, int n)  
	{
	this.setBackground(Color.white);
	this.setSize(700, 500);
	if ( n == 0 )
	{
	g.setColor(Color.MAGENTA);
	g.drawLine(x1, y1, x2, y2);
	g.drawLine(x2, y2, x3, y3);
	g.drawLine(x3, y3, x1, y1);
	return;     
	    }
	    
	int xa, ya, xb, yb, xc, yc;   
	    xa = (x1 + x2) / 2;           
	    ya = (y1 + y2) / 2;
	    xb = (x1 + x3) / 2;
	    yb = (y1 + y3) / 2;
	    xc = (x2 + x3) / 2;
	    yc = (y2 + y3) / 2;
	    Triangulo(g, x1, y1, xa, ya, xb, yb, n - 1);
	    Triangulo(g, xa, ya, x2, y2, xc, yc, n - 1);
	    Triangulo(g, xb, yb, xc, yc, x3, y3, n - 1);
	}
	
	public void paint(Graphics g)    
	{
		int recursions = 3;
		
	    Triangulo(g, 319, 0, 0, 479, 639, 479, recursions);
	}      
}

    


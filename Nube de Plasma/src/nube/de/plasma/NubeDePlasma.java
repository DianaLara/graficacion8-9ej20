package nube.de.plasma;
import java.awt.Color;

/**
 *
 * @author Diana
 */
public class NubeDePlasma {
    
    public static void plasma(double x,  double y,  double size, double stddev,
                              double c1, double c2, double c3,   double c4) {

      
        if (size <= 0.001) return;

        //calcula el nuevo color de punto medio usando desplazamiento aleatorio
        double displacement = StdRandom.gaussian(0, stddev);
        double cM = (c1 + c2 + c3 + c4) / 4.0 + displacement;

        // 
        Color color = Color.getHSBColor((float) cM, 0.8f, 0.8f);
        StdDraw.setPenColor(color);
        StdDraw.filledSquare(x, y, size);

        double cT = (c1 + c2) / 2.0;  
        double cB = (c3 + c4) / 2.0;    
        double cL = (c1 + c3) / 2.0;   
        double cR = (c2 + c4) / 2.0;   

        plasma(x - size/2, y - size/2, size/2, stddev/2, cL, cM, c3, cB);
        plasma(x + size/2, y - size/2, size/2, stddev/2, cM, cR, cB, c4);
        plasma(x - size/2, y + size/2, size/2, stddev/2, c1, cT, cL, cM);
        plasma(x + size/2, y + size/2, size/2, stddev/2, cT, c2, cM, cR);
    }



    public static void main(String[] args) {

        // elije colores de esquina iniciales al azar entre 0 y 1
        double c1 = StdRandom.uniform();
        double c2 = StdRandom.uniform();
        double c3 = StdRandom.uniform();
        double c4 = StdRandom.uniform();

        // controla la variación en el color
        double stddev = 1.0;
        plasma(0.5, 0.5, 0.5, stddev, c1, c2, c3, c4);
    }
    
    
    
    
}

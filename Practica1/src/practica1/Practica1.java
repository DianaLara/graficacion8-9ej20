/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica1;

//import com.sun.corba.se.impl.encoding.CodeSetConversion;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.WindowConstants;


/**
 *
 * @author Diana
 */
public class Practica1 implements ActionListener {
    JFrame ventana;
    JTextField tflNombre;
    JLabel lblNombre;
    JButton btnSaludar;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Practica1 app = new Practica1();
        app.run();
        //MY EASY OPTION
       // String name = JOptionPane.showInputDialog(null, "Escribe un nombre para saludar:");
       // JOptionPane.showMessageDialog(null,"Hola "+name+"!");
     }
    
   void run(){
       ventana = new JFrame("Practica 1");
       ventana.setLayout(new FlowLayout());
       ventana.setSize(300, 200); 
       
       
       JLabel lblNombre = new JLabel("Escriba su nombre");
       JTextField tfNombre = new JTextField(20);
       JButton btnSaludar = new JButton("Saludar");
       
       //Forma 1: 
       //Implementando el ActionListener
       //btnSaludar.addActionListener(this
       
       //Forma 2:
       btnSaludar.addActionListener(new ActionListener(){
       public void actionPerformed(ActionEvent e){
          JOptionPane.showMessageDialog(ventana,"Hola "+tfNombre.getText()+"!"); 
       }
    });
       
       ventana.add(lblNombre);
       ventana.add(tfNombre);
       ventana.add(btnSaludar);
       
       ventana.setVisible(true);
       ventana.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
       
    }
       
       public void actionPerformed(ActionEvent e){
       JOptionPane.showMessageDialog(ventana,"Hola "+this.tfNombre.getText());
        
       }
}

